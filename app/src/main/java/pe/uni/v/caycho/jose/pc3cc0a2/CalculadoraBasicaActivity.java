package pe.uni.v.caycho.jose.pc3cc0a2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

public class CalculadoraBasicaActivity extends AppCompatActivity {

    TextView tvNumber;

    Button btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9, btn0,
    btnIgual, btnSumar, btnRestar, btnMultiplicar, btnDividir,
    btnEliminar,
    btnAtras;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculadora_basica);


        tvNumber = findViewById(R.id.text_view_number);

        btn1 = findViewById(R.id.btn_1);
        btn2 = findViewById(R.id.btn_2);
        btn3 = findViewById(R.id.btn_3);
        btn4 = findViewById(R.id.btn_4);
        btn5 = findViewById(R.id.btn_5);
        btn6 = findViewById(R.id.btn_6);
        btn7 = findViewById(R.id.btn_7);
        btn8 = findViewById(R.id.btn_8);
        btn9 = findViewById(R.id.btn_9);
        btn0 = findViewById(R.id.btn_0);

        btnIgual = findViewById(R.id.btn_igual);
        btnSumar = findViewById(R.id.btn_sumar);
        btnRestar = findViewById(R.id.btn_restar);
        btnMultiplicar = findViewById(R.id.btn_multiplicar);
        btnDividir = findViewById(R.id.btn_dividir);

        btnEliminar = findViewById(R.id.btn_eliminar);

        btnAtras = findViewById(R.id.btn_atras);

        btn1.setOnClickListener(v -> {
            String sNumero = btn1.getText().toString();
            tvNumber.setText(sNumero);
        });

        btn2.setOnClickListener(v -> {
            String sNumero = btn2.getText().toString();
            tvNumber.setText(sNumero);
        });

        btn3.setOnClickListener(v -> {
            String sNumero = btn3.getText().toString();
            tvNumber.setText(sNumero);
        });

        btn4.setOnClickListener(v -> {
            String sNumero = btn4.getText().toString();
            tvNumber.setText(sNumero);
        });

        btn5.setOnClickListener(v -> {
            String sNumero = btn5.getText().toString();
            tvNumber.setText(sNumero);
        });

        btn6.setOnClickListener(v -> {
            String sNumero = btn6.getText().toString();
            tvNumber.setText(sNumero);
        });

        btn7.setOnClickListener(v -> {
            String sNumero = btn7.getText().toString();
            tvNumber.setText(sNumero);
        });

        btn8.setOnClickListener(v -> {
            String sNumero = btn8.getText().toString();
            tvNumber.setText(sNumero);
        });

        btn9.setOnClickListener(v -> {
            String sNumero = btn9.getText().toString();
            tvNumber.setText(sNumero);
        });

        btn0.setOnClickListener(v -> {
            String sNumero = btn0.getText().toString();
            tvNumber.setText(sNumero);
        });

        btnSumar.setOnClickListener(v -> {
            tvNumber.setText("");
            //
        });

        btnRestar.setOnClickListener(v -> {
            tvNumber.setText("");
            //
        });

        btnMultiplicar.setOnClickListener(v -> {
            tvNumber.setText("");
            //
        });

        btnDividir.setOnClickListener(v -> {
            tvNumber.setText("");
            //
        });

        btnAtras.setOnClickListener(v -> {
            Intent intent = new Intent(CalculadoraBasicaActivity.this, SeleccionarCalculadoraActivity2.class);
            startActivity(intent);
            finish();
        });
    }
}