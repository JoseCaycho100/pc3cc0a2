package pe.uni.v.caycho.jose.pc3cc0a2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    ImageView imageView;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);

        imageView = findViewById(R.id.image_view);
        textView = findViewById(R.id.text_view);

        Animation animation_iv = AnimationUtils.loadAnimation(MainActivity.this, R.anim.image_view_animation_rotate);
        Animation animation_tv = AnimationUtils.loadAnimation(MainActivity.this, R.anim.text_view_animation_appear);

        imageView.setAnimation(animation_iv);
        textView.setAnimation(animation_tv);

        new CountDownTimer(5000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

            }

            @Override
            public void onFinish() {
                Intent next = new Intent(MainActivity.this, SeleccionarCalculadoraActivity2.class);
                startActivity(next);
            }
        }.start();

    }
}