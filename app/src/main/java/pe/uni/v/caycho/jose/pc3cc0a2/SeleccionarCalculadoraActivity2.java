package pe.uni.v.caycho.jose.pc3cc0a2;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;
import android.widget.RadioButton;

public class SeleccionarCalculadoraActivity2 extends AppCompatActivity {

    RadioButton rbBasico, rbCientifica, rbProgramador;
    Button btnAceptar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seleccionar_calculadora2);

        rbBasico = findViewById(R.id.radio_button_basico);
        rbCientifica = findViewById(R.id.radio_button_cientifica);
        rbProgramador = findViewById(R.id.radio_button_programador);
        btnAceptar = findViewById(R.id.button_aceptar);

        btnAceptar.setOnClickListener(v -> {
            if(rbBasico.isChecked()){
                Intent intent = new Intent(SeleccionarCalculadoraActivity2.this, CalculadoraBasicaActivity.class);
                startActivity(intent);
                finish();
                return;
            }
            if(rbProgramador.isChecked() || rbCientifica.isChecked()){
                AlertDialog.Builder builder = new AlertDialog.Builder(SeleccionarCalculadoraActivity2.this);
                builder.setTitle(R.string.text_builder_title)
                        .setMessage(R.string.text_builder_continuar)
                        .setCancelable(false)
                        .setPositiveButton(R.string.text_positive, (dialog, which) -> dialog.cancel())
                        .setNegativeButton(R.string.text_negative, (dialog, which) -> {
                            moveTaskToBack(true);
                            android.os.Process.killProcess(android.os.Process.myPid());
                            System.exit(1);
                        });
                AlertDialog  alert = builder.create();
                alert.show();
            }
        });
    }
}